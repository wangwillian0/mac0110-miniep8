function compareByValue(x, y)
    numbers = ['2', '3', '4', '5', '6', '7', '8', '9', '1', 'J', 'Q', 'K', 'A']

    a = 0
    b = 0

    for i in 1:13
        if x[1] == numbers[i]
            a += i
        end
        if y[1] == numbers[i]
            b += i
        end
    end
   
    return a < b
end

function compareByValueAndSuit(x, y)
    suits = ['♦', '♠', '♥', '♣']

    a = 0
    b = 0
    n = length(x)
    m = length(y)

    for i in 1:4
        if x[n] == suits[i]
            a += i
        end
        if y[m] == suits[i]
            b += i
        end
    end

    if a == b
        return compareByValue(x, y)
    else
        return a < b
    end 
end
